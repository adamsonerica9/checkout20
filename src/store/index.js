import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import {store, api} from 'hub20-vue-sdk'

export const CHECKOUT_WIDGET_OPEN = 'CHECKOUT_WIDGET_OPEN'
export const CHECKOUT_WIDGET_CLOSE = 'CHECKOUT_WIDGET_CLOSE'
export const CHECKOUT_WIDGET_MINIMIZE = 'CHECKOUT_WIDGET_MINIMIZE'
export const CHECKOUT_APP_SET_COMPLETED = 'CHECKOUT_APP_SET_COMPLETED'
export const CHECKOUT_APP_SET_CANCELED = 'CHECKOUT_APP_SET_CANCELED'
export const CHECKOUT_APP_SET_EXPIRED = 'CHECKOUT_APP_SET_EXPIRED'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

const initialState = () => ({
  isClosed: false,
  isMinimized: false,
  canceled: new Array(),
  completed: new Array(),
  expired: new Array(),
})

const mutations = {
  [CHECKOUT_WIDGET_OPEN](state) {
    state.isClosed = false
    state.isMinimized = false
  },
  [CHECKOUT_WIDGET_CLOSE](state) {
    state.isClosed = true
  },
  [CHECKOUT_WIDGET_MINIMIZE](state) {
    state.isMinimized = true
  },
  [CHECKOUT_APP_SET_CANCELED](state, checkout) {
    state.canceled.push(checkout)
  },
  [CHECKOUT_APP_SET_COMPLETED](state, checkout) {
    state.completed.push(checkout)
  },
  [CHECKOUT_APP_SET_EXPIRED](state, checkout) {
    state.expired.push(checkout)
  },
}

const actions = {
  setServer({dispatch}, url) {
    return dispatch('server/setUrl', url)
  },
  setStore({commit}, storeId) {
    return api.stores.get(storeId).then(({data}) => commit('checkout/STORE_SET', data))
  },
  initialize({commit, dispatch}, {serverUrl, storeId, charge}) {
    return dispatch('coingecko/fetchCoingeckoTokenList')
      .then(() => dispatch('coingecko/setBaseCurrency', charge.currencyCode))
      .then(() => dispatch('setServer', serverUrl))
      .then(() => dispatch('network/initialize'))
      .then(() => dispatch('setStore', storeId))
      .then(() => dispatch('tokens/initialize'))
      .then(() => commit('checkout/CHARGE_SET_DATA', charge))
  },
  cancel({commit}, checkout) {
    commit(CHECKOUT_APP_SET_CANCELED, checkout)
  },
  close({commit}) {
    commit(CHECKOUT_WIDGET_CLOSE)
  },
  minimize({commit}) {
    commit(CHECKOUT_WIDGET_MINIMIZE)
  },
  open({commit}) {
    commit(CHECKOUT_WIDGET_OPEN)
  },
  finish({commit}, checkout) {
    commit(CHECKOUT_APP_SET_COMPLETED, checkout)
  },
}

export default new Vuex.Store({
  modules: store,
  strict: debug,
  plugins: debug ? [createLogger()] : [],
  state: initialState(),
  mutations,
  actions,
})
